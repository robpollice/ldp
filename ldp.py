#!/usr/bin/env python
import numpy as np
import sys as sys
import argparse as ap
import pathlib as pl
import subprocess as sp
from cube import cube
from dftd4.utils import extrapolate_c8_coeff

#If you use this code for your research, please cite the following publication:
#Pollice, R.; Chen, P. Angew. Chem. Int. Ed. 2019, 58, Early View. DOI: 10.1002/anie.201905439

# Define external program directories
# These paths need to be adjusted!
mwfn = pl.Path("/cluster/home/pollicer/bin/multiwfn")
dftd3 = pl.Path("/cluster/home/pollicer/bin/dftdn/d3/dftd3")
dftd4 = pl.Path("/cluster/home/pollicer/bin/dftdn/d4/dftd4")
cwd = pl.Path.cwd()

# Define constants
conv_hartree = 627.509474 # conversion to kcal/mol; taken from https://en.wikipedia.org/wiki/Hartree (20.09.2018)
conv_bohr = 0.52917721067 # conversion from bohr to angstrom; taken from https://en.wikipedia.org/wiki/Bohr_radius (20.09.2018)

def disp(xyz, method, charge=0):
	# Run DFTD3
	if method.casefold() == "d3".casefold():
		d3_out = sp.check_output([dftd3, xyz, "-func", "b3-lyp", "-bjm"])
		d3_out = d3_out.decode('utf-8')
		# Read output
		start = "#               XYZ [au]               R0(AA) [Ang.]  CN       C6(AA)     C8(AA)   C10(AA) [au]"
		end = "molecular C6(AA) [au]"
		read = 0
		d3 = []
		for line in d3_out.splitlines():
			if start in line and read == 0:
				read = 1
				continue
			elif end in line and read == 1:
				break
			elif read == 1:
				if line.split() != []:
					d3.append([float(line.split()[7]), float(line.split()[8])])
				else:
					continue
		return d3
	
	# Run DFTD4
	elif method.casefold() == "d4".casefold():
		d4_out = sp.check_output([dftd4, xyz, "-c", str(charge), "--molc6"])
		d4_out = d4_out.decode('utf-8')
		# Read output
		start = "#   Z        covCN         q      C6AA      α(0)"
		end = " Mol. C6AA /au"
		read = 0
		d4 = []
		iat = []
		for line in d4_out.splitlines():
			if start in line and read == 0:
				read = 1
				continue
			elif end in line and read == 1:
				break
			elif read == 1:
				if line.split() != []:
					d4.append([float(line.split()[5]), 0.])
					iat.append(int(line.split()[1]))
				else:
					continue
		
		for ni in range(len(d4)):
			d4[ni][1] = extrapolate_c8_coeff(d4[ni][0], iat[ni], iat[ni])

		return d4

def dens(xyz, grid):
	# Convert xyz to promolecular cube
	sp.run(["cp", "-rf", mwfn / "examples/atomwfn", cwd])
	mwfn_input = "5\n-1\n1\n" + grid + "\n2\nq\n"
	sp.run([mwfn / "Multiwfn", xyz], input=mwfn_input, encoding='ascii')
	sp.run(["mv", cwd / "density.cub", cwd / "dens.cube"])
	sp.run(["rm", "-rf", cwd / "atomwfn"])
	sp.run(["rm", "-rf", cwd / "wfntmp"])
	return

def pml(xyz, iso):
	with open(cwd / "ldp.pml", 'w') as f:
		f.write("load " + xyz + "\n")
		f.write("\n")
		f.write("hide everything\n")
		f.write("show sticks\n")
		f.write("show spheres\n")
		f.write("set stick_radius, .06\n")
		f.write("set sphere_scale, .18\n")
		f.write("set sphere_scale, .10, elem H\n")
		f.write("set bg_rgb=[1, 1, 1]\n")
		f.write("set stick_quality, 50\n")
		f.write("set sphere_quality, 4\n")
		f.write("color gray50, elem C\n")
		f.write("color red, elem O\n")
		f.write("color slate, elem N\n")
		f.write("color gray98, elem H\n")
		f.write("set stick_color, black\n")
		f.write("set ray_trace_mode, 1\n")
		f.write("set ray_texture, 2\n")
		f.write("set antialias, 3\n")
		f.write("set ambient, 0.5\n")
		f.write("set spec_count, 5\n")
		f.write("set shininess, 50\n")
		f.write("set specular, 1\n")
		f.write("set reflect, .1\n")
		f.write("set dash_gap, 0\n")
		f.write("set dash_color, black\n")
		f.write("set dash_gap, .15\n")
		f.write("set dash_length, .05\n")
		f.write("set dash_round_ends, 0\n")
		f.write("set dash_radius, .05\n")
		f.write("set fog, off\n")
		f.write("ray\n")
		f.write("\n")
		f.write("load dens.cube, densf\n")
		f.write("load ldp.cube, ldpf\n")
		f.write("\n")
		f.write("preset.ball_and_stick(selection='all')\n")
		f.write("isosurface dens, densf, " + str(iso) + "\n")
		f.write("ramp_new ramp, ldpf, [5.0,50.0,95.0], [[0.333,0.282,0.757],[0.949,0.949,0.949],[0.694,0.004,0.153]]\n")
		f.write("set surface_color, ramp, dens\n")
		f.write("set two_sided_lighting, value=1\n")
		f.write("set transparency, 0.25\n")
		f.write("set surface_quality, 1\n")
		f.write("\n")
		f.write("rebuild\n")
		f.write("\n")
		f.write("turn x,0\n")
		f.write("turn y,0\n")
		f.write("turn z,0\n")
		f.write("\n")
		f.write("png ldp.png, width=1600, height=1200, dpi=300, ray=1\n")
		f.write("\n")
		f.write("quit\n")
		f.write("\n")
	return

def main():
	# Parse arguments
	parser = ap.ArgumentParser()
	parser.add_argument("xyz", help="XYZ File of Input Structure", type=str)
	parser.add_argument("-c", "--charge", help="Molecular charge (only for D4)", type=int, default=0)
	parser.add_argument("-u", "--cube", help="CUBE File of Input Structure", type=str, default=None)
	parser.add_argument("-d", "--disp", help="Dispersion Correction: d3=Grimme D3 Model, d4=Grimme D4 Model", type=str, default="d4")
	parser.add_argument("-g", "--grid", help="Grid Quality: 1=Low, 2=Medium, 3=High", type=str, default="1")
	parser.add_argument("-i", "--iso", help="Isosurface, recommended values: 0.0023 (promolecular density), 0.0100 (molecular density)", type=float, default=None)
	args = parser.parse_args()
	
	# Choose default isovalue if not provided
	if args.iso == None:
		if args.cube == None:
			args.iso = 0.00230 # reproduces PBE/def2-SVPD volumes having an isovalue of 0.00100
		else:
			args.iso = 0.00100
	
	# Determine file ending
	form = pl.Path(args.xyz).suffix
	if form != ".xyz":
		raise RuntimeError("Unsupported file type.")
	
	# Load XYZ
	atoms = np.genfromtxt(pl.Path(args.xyz).resolve(), delimiter=None, usecols = (0), skip_header = 2, dtype=str)
	xyz = np.genfromtxt(pl.Path(args.xyz).resolve(), delimiter=None, usecols = (1, 2, 3), skip_header = 2)
	xyz /= conv_bohr
	natoms = len(xyz)
	
	# Calculate Cn coefficients
	cn = disp(pl.Path(args.xyz).resolve().name, args.disp, args.charge)
	
	# Save Cn coefficients to file
	with open(cwd / "ldp_c6.out", 'w') as f:
		for ni in range(natoms):
			f.write(format(str(atoms[ni]), '>3s') + "  " + format(format(cn[ni][0], '.4f'), '>12s') + "  " + format(format(cn[ni][1], '.4f'), '>12s') + "\n")

	# Load density
	if args.cube == None:
		# Create promolecular density
		dens(pl.Path(args.xyz).resolve(), args.grid)
		ldp = cube(cwd / "dens.cube")
	else:
		cub = pl.Path(args.cube).resolve()
		ldp = cube(cub)
	
	# Generate grid
	xgrid = np.linspace(ldp.orig[0], ldp.orig[0] + ldp.nx*ldp.x0[0], ldp.nx + 1)
	ygrid = np.linspace(ldp.orig[1], ldp.orig[1] + ldp.ny*ldp.y0[1], ldp.ny + 1)
	zgrid = np.linspace(ldp.orig[2], ldp.orig[2] + ldp.nz*ldp.z0[2], ldp.nz + 1)
	
	# Calculate pint
	pint = np.zeros((ldp.nx, ldp.ny, ldp.nz))
	for ix in range(ldp.nx):
		for iy in range(ldp.ny):
			for iz in range(ldp.nz):
				for ni in range(natoms):
					ij = np.sqrt(np.sum((xyz[ni] - np.array([xgrid[ix],ygrid[iy],zgrid[iz]]))**2))
					pint[ix,iy,iz] += (np.sqrt(cn[ni][0])/((ij)**3) + np.sqrt(cn[ni][1])/((ij)**4))*np.sqrt(conv_hartree)
	
	# Save cube file
	ldp.write(pint)
	ldp.dump(cwd / "ldp.cube", "LDP:")
	
	# Save pml template
	pml(pl.Path(args.xyz).name, args.iso)

if __name__ == "__main__":
	main()
