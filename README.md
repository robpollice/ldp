# LDP

Python 3 scripts to compute London dispersion potential maps and P_int values of molecules and functional groups. Usage of the scripts requires Multiwfn and DFT-D3 or DFT-D4. In addition, two examples are provided.

If you use this code for your research, please cite the following publication:

Pollice, R.; Chen, P. *Angew. Chem. Int. Ed.* **2019**, *58*, 9758-9769. DOI: 10.1002/anie.201905439

# DFTD4

The `dftd4` program needs to be installed first, followed by installation of the corresponding python wrapper (https://github.com/dftd4/dftd4/tree/master/python). For installation of `dftd4` please follow the instructions on the corresponding GitHub repository (https://github.com/dftd4/dftd4). To install the python wrapper, download/clone the `dftd4` repository, go to the `dftd4/python` directory and run `python setup.py install`. Afterwards, `dftd4` should be ready to run together with this code.

# Usage

The paths to the external programs need to be adjusted before running the scripts. For the following examples, it is assumed that current working directory is in the examples folder and the relative paths are equal to the repository after extracting the ZIP archives.

## Example 1 - LDP of Benzene

`python ../ldp.py ./benzene.xyz -c 0 -u ./dens.cube -i 0.00100`

## Example 2 - P_int of Methyl

`python ../ded.py ./me.xyz ./vdw.txt --anchor ./anchor.txt --charge 0 --disp d4`

## Creating vdw.txt

To use ded.py, a file containing the points on the vdW surface (vdw.txt in the example) is required. This file can be created using Multiwfn. When an orbital file (WFN or MOLDEN) is opened with Multiwfn, this can be done using main function 12 (Quantitative analysis of molecular surface). To deactivate calculation of the ESP on the surface, enter 2 and then -2. Afterwards, entering 0 creates the isosurface of electron density with default parameters (isovalue of 0.001, distance between points of 0.25 Bohr). The computed data can be saved as TXT file by entering 7 (Export all surface vertices to vtx.txt in current folder
). This file can be directly used by the ded.py script for further processing.

## Visualizing LDP Maps

The output of the LDP generation is a CUBE file. It can be visualized with several programs in very much the same way as electrostatic potential (ESP) maps are displayed. Hence, I recommend to find tutorials on how to visualize ESP maps for creating images of the LDP map.
